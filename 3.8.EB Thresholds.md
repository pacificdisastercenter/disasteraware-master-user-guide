## Event Brief Triggers

Event Brief provides automated, near-real-time hazard impact assessments for hazards impacting (or predicted to impact) population. An Event Brief will be made available (triggered) when DisasterAWARE detects that population fall within the areas listed for each hazard below. 

 

Note: Event brief provides enhanced and expanded exposure and impact information to help you quickly answer *What happened? When and where did it happen? How bad is it?* However, if a hazard does not meet the trigger requirements listed below to issue an Event Brief, preliminary exposure estimates can be accessed by selecting the “More Information” button under that hazard’s Information tab. Be sure to revisit the hazard to see if an Event Brief has been created and for updates.   
 

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 79%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Hazard</strong> </th>
<th><strong>Event Brief Triggers</strong> (Population exposed to the following.) </th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Tropical Cyclone </td>
<td>Minor Damage; power out and higher - Estimated Wind Impacts (TOAS Model) </td>
</tr>
<tr class="even">
<td>Avalanche </td>
<td>10km buffer (PDC) </td>
</tr>
<tr class="odd">
<td>Biomedical </td>
<td>General affected area (PDC)) </td>
</tr>
<tr class="even">
<td>Drought </td>
<td>General affected area (PDC) </td>
</tr>
<tr class="odd">
<td>Earthquake </td>
<td>Moderate Shaking (V) and stronger – Shaking Intensity (USGS ShakeMap Model) </td>
</tr>
<tr class="even">
<td>Extreme Temperature </td>
<td>General affected area (PDC) </td>
</tr>
<tr class="odd">
<td>Flood </td>
<td><p>US – Flood Warning area (NWS) </p>
<p>Canada – Flood Warning area (Canada Met) </p>
<p>Hawaii – Flood hazard area (NWS) </p>
<p>Global – Predictive Flood Warning area (NASA/PDC) or general affected area (PDC) </p></td>
</tr>
<tr class="even">
<td>High Surf </td>
<td><p>Hawaii – High Surf alert area (NWS) </p>
<p>Global – General affected area (PDC) </p></td>
</tr>
<tr class="odd">
<td>High Winds </td>
<td><p>Hawaii – High Winds alert area (NWS) </p>
<p>Global – General affected area (PDC) </p></td>
</tr>
<tr class="even">
<td>Landslide </td>
<td>Predictive Landslide hazard area (NASA/PDC) or general affected area (PDC) </td>
</tr>
<tr class="odd">
<td>Marine </td>
<td>General affected area (PDC) </td>
</tr>
<tr class="even">
<td>Storm </td>
<td>General affected area (PDC) </td>
</tr>
<tr class="odd">
<td>Tornado </td>
<td><p>US – Warning and Watches area (NWS) </p>
<p>Global – General affected area (PDC) </p></td>
</tr>
<tr class="even">
<td>Tsunami </td>
<td>Coastal inland buffer refined by local tsunami zones were available (PDC) </td>
</tr>
<tr class="odd">
<td>Volcano </td>
<td>10km buffer refined by additional hazard data for specific volcanos were available (PDC) </td>
</tr>
<tr class="even">
<td>Wildfire </td>
<td>Global – 24-hour Wildfire Activity Area (PDC) or manually defined general affected area (PDC) if not available <br />
USA – Current Wildfire Perimeter (NIFC) or 24-hour Wildfire Activity Area (PDC) if not available </td>
</tr>
<tr class="odd">
<td>Winter Storm </td>
<td>US – Winter Storm Warning area (NWS) <br />
Global – General affected area (PDC) </td>
</tr>
</tbody>
</table>

 

# 
