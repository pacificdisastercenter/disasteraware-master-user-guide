## Filter results by country

By default, Event Brief displays estimates for the entire geographic area affected by a hazard. If there is more than one country affected by a hazard, you can filter information based on single country. Click the **Set Filter by Country** dropdown.

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/3.2.EB Filter Results-Media/media/image1.png" style="width:3.68862in;height:1.49153in" alt="Graphical user interface Description automatically generated" />

You can remove the country filter by selecting the Set Filter by Country dropdown again.
