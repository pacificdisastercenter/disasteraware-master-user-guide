## Hazard Exposure tab

The Hazard Exposure tab is activated by default when you first load an Event Brief. When you navigate between tabs, the map and the supporting information changes to reflect the subject of the tab.

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/3.3.EB Hazard exposure tab-Media/media/image1.jpeg" style="width:7in;height:3.27778in" alt="Graphical user interface, chart Description automatically generated" />

### Map navigation

You can navigate the Event Brief map in the following ways:

- Click the map with your mouse, then hold and drag your cursor to move the map in the desired direction.

- Select the on-screen zoom in (+) and out (-) buttons in the lower right corner of the map to change your zoom extent, or double-click on the map to zoom in.

- Add or remove visible data layers displayed on the map by selecting an item from the list on the bottom left corner of the map. A blue highlight means the data layer is visible.

- Use the zoom options described above to view individual points in a clustered dataset. Note: Clustered data is depicted by a light blue circle surrounding the clustered information.
