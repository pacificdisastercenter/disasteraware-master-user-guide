## Share your products in DisasterAWARE 

Users with administrative privileges have the ability to add and edit hazard products in DisasterAWARE, such as a File (e.g. photo, map, SitRep, report) and URLs (e.g. link to website or bookmark URL). To learn more about administrative capabilities see the section on <em>System Administration</em>. If you do not have administrator privileges, but wish to share a product, contact <response@pdc.org> to have your file uploaded to the system.

### Sharing an existing hazard product

You can share any product associated with a hazard in the system by clicking a **Hazard** icon on the map and then the **Products** link from the map tooltip. Navigate to the products folder of interest.

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/5.3 Share your products-Media/media/image1.png" style="width:6.39596in;height:2.73918in" />

Once you have identified a product you wish to share, click the **Info** icon. Then copy/paste the link provided to share with other authorized users (requires log in).

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/5.3 Share your products-Media/media/image2.png" style="width:5.66441in;height:3.05087in" />
