**RELEASE NOTES**

**DisasterAWARE<sup>®</sup> v 6.8.4 |** Release Date: 23 July 2021

New Features 
============

**Configure Notification Popups: The Hazard notification popup that appears in the upper corner may now be turned off by users. Hazard notifications may be disabled from the User Preferences panel.**

**Streaming Video: DisasterAWARE users may now enable up to 10 static camera and/or video streaming locations simultaneously.**

**Event Brief (Beta): The new Event Brief report (currently released in beta), provides** critical, life-saving information to aid rapid response during large-scale hazards that pose potentially devastating impacts. Access Event Brief by clicking a hazard icon on the map and choosing Event Brief from the map tip.

Issues Resolved
===============

**Bookmark | Figure Eraser: Addressed an issue in which the bookmark eraser tool would remain enabled after the panel was closed.**

**Bookmark | Layer filters: User-applied layer filters (where available) will now be saved to bookmarks and be applied when the bookmark is enabled.**

**Drawing | Firefox Annotations: Annotations in Firefox will now be vertically centered in the annotation box rather than top-aligned.**

**Identify | Zoom To: Fixed an issue where the “Zoom To” feature would not work appropriately for some layers within summary reports created with the Identify tool.**

**Layer checker: Fixed an issue with DisasterAWARE and the MapService Layer Checker where layer status was not updating appropriately.**

**Legend | Images: Fixed an issue where static images used in the Legend may not have appeared correctly when collapsed/expanded.**

**Search: The Search bar will no longer appear in the Hazard Inspector palette if the Map Search feature was inspected before a hazard.**

**SmartAlert Notifications | Alert Areas: When a user has custom alert areas disabled, the “Alert Area(s)” section will now update appropriately.**

**Tooltips: Tooltips will no longer ‘stick’ and remain visible when buttons are clicked on mobile deployments.**

Known Issues
============

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief | Historical Hazards:** Historical hazard data is limited and may not match user expectations.

**Area Brief | Map:** The map displayed inside the *Infrastructure & Critical Facilities* section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Area Brief | RVA:** Risk & Vulnerability Assessment (RVA) layers presented may not match the legends in the report.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to [<span class="underline">http://disasteraware.pdc.org/help/How\_To\_Disable\_AdBlockers.pdf</span>](http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf)

**Burmese language on animated layers:** Burmese language does not translate on some animated layers with a time scale and instead display “NaNh.”

**Drawing | Anti-meridian:** Drawing elements in bookmarks that cross the 180<sup>th</sup> meridian may not copy appropriately if duplicated using the bookmark figure inspector.

**Drawing | Large figures:** Drawing elements drawn at a high map extent may undisplay early as the user pans the map. To prevent this, draw and maintain drawing features at a lower map view.

**Drawing | Navigation with eraser activated:** Navigation is disabled when the eraser tool is activated. To resume map navigation, pan or zoom, disable the eraser tool and try again.

**Drawing | Tools disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in.

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase “I” may look like a lowercase “L.” Users can verify spelling if needed by changing the zoom level of the browser and temporarily zoom in to view the text.

**Fonts in DMRS**: The appearance of English user interface elements in DMRS may not appear correctly if users install & use the Google Chrome extension “Myanmar Font Tools” for ZAWGYI encoding standards. This issue only affects users on ZAWGYI computers.

**Firefox | Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Layer checker | Google mapping engine:** DisasterAWARE may not update the background layer’s status when ‘Google’ is set as the Preferred Mapping Engine.

**Print Service | Map Tips:** Map Tips are not supported for Prints at this time.

**Print Service | Paper size:** A0 and A1 paper sizes (poster sizes) are not currently available.

**Safari | Logout:** If a Safari user clicks the back arrow after logging out the application will temporarily be reloaded. We recommend using the Firefox or Chrome browsers instead.

**Safari | Save to File**: The bookmark option to “Save to File” feature is currently not available for Safari users.

**SmartAlert Notifications | Stop:** Where available, when a user replies “Stop” to a text (SMS)

SmartAlert, the response they receive is in English, regardless of which language is selected.

**SmartAlert Notifications | Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for SmartAlert notifications.

**SmartAlert Notifications | Assets:** Users who have their alert area set as “Global” but who have active Asset Providers may only receive notifications for hazards that intersect their active tracked Assets. To receive “Global” notifications, users should delete the Asset Providers and stop tracking assets.
