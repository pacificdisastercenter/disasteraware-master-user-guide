**RELEASE NOTES**

**DisasterAWARE<sup>®</sup> v 7.3.1 |** Release Date: 29 Mar 2022

**Please Note:** All changes for 7.3.0 and 7.3.1 were publicly released with version 7.3.1 and are documented as part of the 7.3.1 Release Notes below.

Issues Resolved
===============

**Area Brief | Text alignment: The alignment of lat/long coordinates that display for new Area Brief reports will now show without overlapping the text.**

**Bookmark | Figure list: Made improvements for the display of Figures within bookmarks where the complete list of drawn figures will display appropriately with different screen sizes.**

**Camera clusters: Fixed an issue with camera clusters that inappropriately moved as the user panned the map contributing to confusion on camera availability.**

**Drawing Figures | Bookmark creation: Fixed an issue where the drawing figures list may have appeared blank after creating a new bookmark.**

**Drawing Figures | Image style: Addressed an issue where users may not be able to interact with added Image figures if the “Scale with Map Zoom” style option was toggled Off.**

**Hazard Filter | Hazard Icons: Aligned hazards into an organized grid and made the hazard icon darker to increase visibility and reduce icon fuzziness.**

**Language change: Addressed an issue where the application would not return to the users starting language if the user changed their language and then returned to their starting language in the same session.**

**Overview map: Fixed a display issue with the overview map in the bottom right corner of DisasterAWARE where the overview map stopped displaying after a zoom change.**

**Print | Username: Fixed an issue with the Print interface where the username did not appear appropriately.**

**Shapefile Import: Addressed an issue that prevented some shapefiles were not able to be imported.**

**User Preferences | Default bookmark: Fixed a UI bug that presented when selecting a Default Bookmark from the User Preferences menu.**

Deprecated
==========

**KML Layer Export: The feature to export KML and GPX files has been removed from the Map panel action menu in accordance with PDC’s Data Use Agreements. KML Exports of drawing figures may still be initiated from the Drawing panel Figures action menu.**

Known Issues
============

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief | Historical Hazards:** Historical hazard data is limited and may not match user expectations.

**Area Brief | Map:** The map displayed inside the *Infrastructure & Critical Facilities* section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Area Brief | RVA:** Risk & Vulnerability Assessment (RVA) layers presented may not match the legends in the report.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to [<span class="underline">http://disasteraware.pdc.org/help/How\_To\_Disable\_AdBlockers.pdf</span>](http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf)

**Burmese language on animated layers:** Burmese language does not translate on some animated layers with a time scale and instead display “NaNh.”

**Drawing | Anti-meridian:** Drawing elements in bookmarks that cross the 180<sup>th</sup> meridian may not copy appropriately if duplicated using the bookmark figure inspector.

**Drawing | Large figures:** Drawing elements drawn at a high map extent may undisplay early as the user pans the map. To prevent this, draw and maintain drawing features at a lower map view.

**Drawing | Navigation with eraser activated:** Navigation is disabled when the eraser tool is activated. To resume map navigation, pan or zoom, disable the eraser tool and try again.

**Drawing | Tools disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in.

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase “I” may look like a lowercase “L.” Users can verify spelling if needed by changing the zoom level of the browser and temporarily zoom in to view the text.

**Firefox | Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Layer checker | Google mapping engine:** DisasterAWARE may not update the background layer’s status when ‘Google’ is set as the Preferred Mapping Engine.

**Print Service | Map Tips:** Map Tips are not supported for Prints at this time.

**Print Service | Paper size:** A0 and A1 paper sizes (poster sizes) are not currently available.

**Safari | Logout:** If a Safari user clicks the back arrow after logging out the application will temporarily be reloaded. We recommend using the Firefox or Chrome browsers instead.

**Safari | Save to File**: The bookmark option to “Save to File” feature is currently not available for Safari users.

**SmartAlert Notifications | Stop:** Where available, when a user replies “Stop” to a text (SMS)

SmartAlert, the response they receive is in English, regardless of which language is selected.

**SmartAlert Notifications | Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for SmartAlert notifications.

**SmartAlert Notifications | Assets:** Users who have their alert area set as “Global” but who have active Asset Providers may only receive notifications for hazards that intersect their active tracked Assets. To receive “Global” notifications, users should delete the Asset Providers and stop tracking assets.

**Virtual layers | Selection bar:** If users have select themes enabled (PDC, Light, or Dark) then the Virtual Layer bar at the bottom of the map viewer may not be easily used to determine the active selection. Users may pause the animation and manually choose a selection to determine which selection is actively displaying in the map viewer.

**Web Map Service (WMS) | Asynchronous requests:** When a user selects a location, a request is made in parallel to each enabled WMS layer to see if there is a feature at that location. The first layer to respond with a feature is the displayed layer. Making requests serially in an order defined by the visual order of the layers may increase response times unnecessarily.

**Web Map Service (WMS) | Coordinates:** WMS 1.3 specification flips coordinates in a northing then easting orientation (minY, minx, maxY, maxX) which differs than standard orientation (minX, minY, maxX, maxY). DisasterAWARE flips the coordinates for WMS 1.3 and projection 4326 to accommodate this behavior.

**Web Map Service (WMS) | Undeclared virtual layers:** Imported layers may appear as independent layers in the table of contents (ToC) and may display as both a child-of and independent layer in the Table of Contents. Layer inspection may differ depending on the layer(s) enabled when the inspection event occurs.

**Web Map Service (WMS) | Symbology:** Icons displayed in Map Tips and the Feature Inspector may not match the symbol displayed on the map.

**Web Map Service (WMS) | Feature list:** The Feature List for WMS services will only show features that the user discovered by clicking on the map. The feature list may not show all features that are on the map like other feature layers do.
