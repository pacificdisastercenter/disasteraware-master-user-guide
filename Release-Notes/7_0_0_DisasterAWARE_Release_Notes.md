**RELEASE NOTES**

**DisasterAWARE<sup>®</sup> v 7.0 |** Release Date: 1 Dec 2021

New Features 
============

**Action Menu:** The action menu, represented by three dots at the bottom of each menu panel \[…\], has been relocated. It was moved from bottom of the panel the top right corner of the panel. When clicked, the action menu will load additional options.

**Forgot Password Experience:** The DisasterAWARE Forgot Password experience has changed. Now, when the user chooses forgot password they’re prompted for their username. On submission, an email will be sent to the user’s email entered on registration with a link that will allow the user to change their password.

**Hazards | Hazard category:** Hazard icon symbols, color coding, and shapes have been overhauled. Colors now represent hazard severity. Please refer to the 7.0 User Guide for additional details.

**Organizations:** Users will see a new option to ‘Reset Preferences’ in their User panel. If clicked, this will overwrite their active preferences to those of their assigned Organization. Organization administrators will have the ability to overwrite PDC defaults for some Preferences which may be adopted by all new Organization members and those who click ‘Reset Preferences.’

**Themes:** There is a default theme that fully integrates DisasterAWARE branding, colors, and iconography. Additional themes have been created including a Dark mode, Light mode.

Issues Resolved
===============

**Disaster Alert | Refresh Products: Addressed an issue where the Refresh Product option appeared in the action menu on Disaster Alert. This was removed because Disaster Alert is a public version of the app for which products are not available.**

**Layer display: Fixed an issue that prevented a layer from fully rendering. This occurred when the user quickly enabled and disabled the layer before all features had fully loaded in the map viewer.**

**Layer status notifications: Addressed an issue where DisasterAWARE may incorrectly report a working layer as offline when the legend was opened.**

**Virtual layers | Extra legend entry: Resolved an issue with the legend that presented when composite virtual layers were enabled. When enabled, the legend content would be duplicated in the legend pane.**

Deprecated
==========

**Dashboard View:** The Dashboard View option has been removed from the User Preference options.

**Hazard Products | Event Log:** The Event Log product type has been removed from DisasterAWARE. Product admin will no longer see this option available to them.

**Hazard Products | Access Restriction:** The ability to restrict access to products has been removed from DisasterAWARE while it undergoes improvement. Product admin will no longer see this option available to them.

Known Issues
============

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief | Historical Hazards:** Historical hazard data is limited and may not match user expectations.

**Area Brief | Map:** The map displayed inside the *Infrastructure & Critical Facilities* section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Area Brief | RVA:** Risk & Vulnerability Assessment (RVA) layers presented may not match the legends in the report.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to [<span class="underline">http://disasteraware.pdc.org/help/How\_To\_Disable\_AdBlockers.pdf</span>](http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf)

**Burmese language on animated layers:** Burmese language does not translate on some animated layers with a time scale and instead display “NaNh.”

**Drawing | Antimeridian:** Drawing elements in bookmarks that cross the 180<sup>th</sup> meridian may not copy appropriately if duplicated using the bookmark figure inspector.

**Drawing | Large figures:** Drawing elements drawn at a high map extent may undisplay early as the user pans the map. To prevent this, draw and maintain drawing features at a lower map view.

**Drawing | Navigation with eraser activated:** Navigation is disabled when the eraser tool is activated. To resume map navigation, pan or zoom, disable the eraser tool and try again.

**Drawing | Tools disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in.

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase “I” may look like a lowercase “L.” Users can verify spelling if needed by changing the zoom level of the browser and temporarily zoom in to view the text.

**Fonts in DMRS**: The appearance of English user interface elements in DMRS may not appear correctly if users install & use the Google Chrome extension “Myanmar Font Tools” for ZAWGYI encoding standards. This issue only affects users on ZAWGYI computers.

**Firefox | Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Layer checker | Google mapping engine:** DisasterAWARE may not update the background layer’s status when ‘Google’ is set as the Preferred Mapping Engine.

**Print Service | Map Tips:** Map Tips are not supported for Prints at this time.

**Print Service | Paper size:** A0 and A1 paper sizes (poster sizes) are not currently available.

**Safari | Logout:** If a Safari user clicks the back arrow after logging out the application will temporarily be reloaded. We recommend using the Firefox or Chrome browsers instead.

**Safari | Save to File**: The bookmark option to “Save to File” feature is currently not available for Safari users.

**SmartAlert Notifications | Stop:** Where available, when a user replies “Stop” to a text (SMS) SmartAlert, the response they receive is in English, regardless of which language is selected.

**SmartAlert Notifications | Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for SmartAlert notifications.

**SmartAlert Notifications | Assets:** Users who have their alert area set as “Global” but who have active Asset Providers may only receive notifications for hazards that intersect their active tracked Assets. To receive “Global” notifications, users should delete the Asset Providers and stop tracking assets.

**Virtual layers | Selection bar:** If users have select themes enabled (PDC, Light, or Dark) then the Virtual Layer bar at the bottom of the map viewer may not be easily used to determine the active selection. Users may pause the animation and manually choose a selection to determine which selection is actively displaying in the map viewer.