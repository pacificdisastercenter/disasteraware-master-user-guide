Release Notes 
=============

**DisasterAWARE<sup>®</sup> v 6.7.2 |** Release Date: 20 October 2020

New features 
------------

**Imported Layers | Refresh interval:** User imported layers will now periodically ‘refresh,’ to account for changes to layer feature data or locations.

**Imported Layers | Transparency:** Some user imported layers may now allow for transparency changes within the application.

Issues resolved
---------------

**Drawing | Eraser highlighting:** Fixed an issue where the border would not change colors to indicate the selected figure when users hovered over the figure with the eraser tool.

**Hazard Palette:** Fixed an issue where the application may enter an inconsistent state and show duplicate hazards when the user filtered active hazards by text name and left the application idle.

**Layer flickering:** Made enhancements to improve “flickering” of Global Layers when a user panned the map.

**Login | Error messages:** Fixed an issue where various error messages did not contain the appropriate informative information to indicate the issue preventing login.

**Logout in multiple tabs:** Fixed an issue that occurred when a user opened multiple tabs via share links for hazards, products, or bookmarks where only the tab the user clicked “Logout” in would “Logout.” Now, all user opened tabs from a session will log out upon clicking “Logout” in any tab.

**Sharing bookmarks: **Fixed an issue that occurred when downloading and sharing bookmarks as a JSON file in which the imported JSON would not display layers shared.

**Translations:** Made enhancements to improve slow translation speeds.

Known Issues
------------

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief:** The map displayed inside the *Infrastructure & Critical Facilities* section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Area Brief:** Risk & Vulnerability Assessment (RVA) layers presented may not match the legends in the report.

**Area Brief | Circle Selector:** The Circle selector tool may not be used to create Area Brief reports.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to <http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf>

**Burmese language on animated layers**: Burmese language does not translate on some animated layers with a time scale and instead displays “NaNh.”

**Drawing tool disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in. 

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase “I” may look like a lowercase “L.” Users can verify spelling if needed by changing the zoom level of the browser and temporarily zoom in to view the text.

**Fonts in DMRS**: The appearance of English user interface elements in DMRS may not appear correctly if users install & use the Google Chrome extension “Myanmar Font Tools” for ZAWGYI encoding standards. This issue only affects users on ZAWGYI computers.

**Firefox | Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Internet Explorer 11 | Stability**: IE11 users may experience slowdowns when moving the map, opening menus, and opening products. Users may also experience intermittent program crashes.

**Internet Explorer 11 | Bookmarks:** Bookmark edits may not appear in IE 11 until the cache is cleared.

**Internet Explorer 11 | Attachments:** IE 11 administrators cannot change a product that has been uploaded (jpg, pdf or txt, for example). If a user cannot switch to a different browser or create a new product with the new file, hide the original product by adding it to the “Recycle Bin” parent folder.

**Internet Explorer 11 | Drawing tools:** In IE 11, the Free Line and Free Polygon tools may shift away from their intended location (cursor position) while being drawn.

**Internet Explorer 11 | InPrivate browsing**: The product list preview will not appear with private browsing enabled (IE InPrivate browsing mode).

**Safari | Save to File**: The bookmark option to “Save to File” feature is currently not available for Safari users.

**SmartAlert Notifications | Stop:** When a user replies “Stop” to a text (SMS) SmartAlert, the response they receive is in English, regardless of which language is selected.

**SmartAlert Notifications | Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for SmartAlert notifications.

**SmartAlert Notifications | Assets:** Users who have their alert area set as “Global” but who have active Asset Providers may only receive notifications for hazards that intersect their active tracked Assets. To receive “Global” notifications, users should delete the Asset Providers and stop tracking assets.

**User Panel:** On the Account section of the User panel, the organization and title is not saved.
