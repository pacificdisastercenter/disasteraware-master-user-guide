**RELEASE NOTES**

**DisasterAWARE<sup>®</sup> v 8.0.11 |** Release Date: 29 Apr 2024

# Issues Resolved 

**Custom placemarks | Default text size:** Changed the default text size to font size 14 to improve visibility on the map viewer.

**Custom placemarks | Moving:** Fixed an issue impacting custom placemarks that were not anchored to the map viewer.

**Custom placemarks | Print products:** Made improvements to prevent placemarks shifting on generated print products.



# Known Issues

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Browser Ad Blockers:** Supplemental information products such as situation reports, map graphics, and other analytical products associated with a hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to [<u>http://disasteraware.pdc.org/help/How\_To\_Disable\_AdBlockers.pdf</u>](http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf)

**Drawing | Large figures:** Drawing objects placed on the map at a very close zoom extent may stop displaying as the user pans the map. To prevent this, draw and maintain drawing features at a lower zoom extent.

**Drawing | Navigation with eraser activated:** Navigation is disabled when the eraser tool is activated. To resume map navigation, pan or zoom, or disable the eraser tool and try again.

**Drawing | Tools disabled:** If drawings cannot be added to the map or problems arise with the drawing tools, the zoom extent may be too high. Zoom out a bit and then try again.

**Domain Change | Logout:** When configuring Disaster Alert to work with an external Domain, once a user logs out of the configured domain they will also be logged out of their session in Disaster Alert.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem, please zoom in.

**Imported layers:** If attempting to import a Map Service already configured in DisasterAWARE, the name of the imported layers may not appear according to the import configuration but rather the existing configuration for the Map Service.

**Language change:** Changing languages in DisasterAWARE may take longer than expected for the app to translate to the selected language. Follow these steps as a workaround: 1) Select the desired language; 2) Select English; 3) Brief pause; and 4) Select the desired language.

**Layer checker | Google mapping engine:** DisasterAWARE may not update the background layer status when ‘Google’ is set as the preferred mapping engine.

**Print Service | Layers:** Due to potential configuration issues, it is recommended to enable only the layers desired for completed print products. Disable other layers from the DisasterAWARE Table of Contents before creating the product with the Print feature. This can help prevent unexpected layers from displaying in the finished Print Product. <span class="mark">Additionally, some layers may not be applied as expected in print products</span>.

**Print Service | Map Background:** Google base maps are not supported in print products. If a print is submitted with a Google Basemap a Bing map will be automatically applied. This may slightly change the appearance of the created product and vary from user expectations.

**Print Service | Map Tips:** The Print feature does not support the printing of Map Tips at this time.

**Print Service | Paper size:** A0 and A1 paper sizes (poster sizes) are not currently available with the Print feature.

**RVA Data:** Data is not rounding or displaying with the percent sign (%) as expected. This is impacting newer data added to the system beginning in 2022.

**RVA Data | No Data:** The legend in DisasterAWARE may be missing the ‘No Data’ legend entry. This data category is drawn as a grey color in the map viewer. The layer draws as expected and the rest of the information is available in the legend (except the ‘No Data’ entry).

**Smart Alert | Email Verification:** When changing the email address configured for Smart Alerts in the User preferences panel, users must verify the new email address through the confirmation link sent to their email. If not verified, Smart Alerts will continue to be sent to the old email address.

**Smart Alert Notifications | Assets:** Users who have their alert area set to Global, yet also have active Asset Providers, may only receive notifications for hazards that intersect their actively tracked assets. To receive Global notifications, users must delete the Asset Providers and stop tracking assets.

**Table of contents | Freezing:** If the Recent Layers folder is open and all the folder layers are enabled, DisasterAWARE may not operate as expected and users may not be able to navigate the Table of Contents from the Layers panel. One workaround is to make sure the “Recent Layers” folder is closed while navigating the Table of Contents and inspecting data layers.

**Virtual layers | Selection bar:** If users have themes enabled (PDC, Light, or Dark), the Virtual Layer bar at the bottom of the map viewer may not be easily used to determine the active selection. Users may pause the animation and manually choose a selection to determine which selection is actively displaying in the map viewer.

**Web Feature Service (WFS) | Feature count:** WFS feature counts are limited to 100 due to bandwidth limitations.

**Web Feature Service (WFS) | Layer style:** WFS does not support custom styles. Therefore, layers are drawn with default icons.

**Web Map Service (WMS) | Asynchronous requests:** When a user selects a location, a request is made in parallel to each enabled WMS layer to see if there is a feature at that location. The first layer to respond to the request is the first layer displayed. Serial requests such as this, where the order is defined by the visual order of the layers, may increase response times unnecessarily.

**Web Map Service (WMS) | Coordinates:** WMS 1.3 specification flips coordinates in a North and then East orientation (minY, minX, maxY, maxX), which differs from a standard orientation (minX, minY, maxX, maxY). DisasterAWARE flips the coordinates for WMS 1.3 and projection 4326 to accommodate this behavior.

**Web Map Service (WMS) | Cursor hover:** The cursor will not change from the hand symbol to the pointer symbol when WMS features are hovered over in DisasterAWARE. Map tips will also not display on feature hover.

**Web Map Service (WMS) | Undeclared virtual layers:** Imported layers may appear as independent layers in the Table of Contents (ToC) as well as a child of a parent. Layer inspection may differ depending on the layer(s) enabled when the inspection event occurs.

**Web Map Service (WMS) | Symbology:** Icons displayed in Map Tips and the Feature Inspector may not match the symbol displayed on the map.

**Web Map Service (WMS) | Feature list:** The Feature List for WMS services will only show features that the user discovered by clicking on the map. The feature list may not show all features that are on the map as other feature layers do.

**Zoom To Tool:** Users who click the map quickly multiple times with the Zoom To tool enabled (magnifying tool option from the right-side navigation menu), may experience undesired behavior. The map will not tile appropriately at the new zoom level. When the user clicks Home from the right-side navigation tools, the hazard clusters will appear expanded.
