**RELEASE NOTES**

**DisasterAWARE<sup>®</sup> v 7.3.3 |** Release Date: 10 May 2022

**Please Note:** All changes for 7.3.2 and 7.3.3 were publicly released with version 7.3.3 and are documented as part of the 7.3.3 Release Notes below.

Issues Resolved
===============

**Asset panel | Delete icon: Fixed the display of the delete icon in the asset providers panel.**

**Hazard clusters | Anti-meridian: Made improvements so hazards will cluster correctly across the date line.**

**Imported file names: Made improvements to the file import functionality so file imports will now better inherit the file name when imported.**

**Layer notifications | User layers: Users who have imported map services from the layer import will now see a layer status notification if their imported layer fails to load when enabled.**

**Login display: DisasterAWARE loading splash screens have been updated to better reflect system branding.**

**Panel loading: Addressed an issue where on first load the menu panels may have had an undesired display.**

**Print jobs: Improved the print feature functionality to better tolerate situations where user drawing figures or layer feature geometry previously prevented a print job from adding to queue.**

**Safari | Features: Safari browser users will no long witness the blue square on the map viewer when features are clicked on the map.**

**Smart Alert™ area display: Fixed an issue where the Smart Alert area would not display when the Info tab was selected from the hazard map tip.**

**User login: DisasterAWARE login and username entry is no longer case sensitive requiring lowercase username entry. If a user enters their username with capitalization the application will transform the username to lowercase on submission.**

**User preferences | Display Hidden Products: The user preference to Display Hidden Products (accessible from the Account Preferences panel) will now be saved appropriately for the user selection.**

Known Issues
============

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief | Historical Hazards:** Historical hazard data is limited and may not match user expectations.

**Area Brief | Map:** The map displayed inside the *Infrastructure & Critical Facilities* section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Area Brief | RVA:** Risk & Vulnerability Assessment (RVA) layers presented may not match the legends in the report.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to [<span class="underline">http://disasteraware.pdc.org/help/How\_To\_Disable\_AdBlockers.pdf</span>](http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf)

**Burmese language on animated layers:** Burmese language does not translate on some animated layers with a time scale and instead display “NaNh.”

**Drawing | Anti-meridian:** Drawing elements in bookmarks that cross the 180<sup>th</sup> meridian may not copy appropriately if duplicated using the bookmark figure inspector.

**Drawing | Large figures:** Drawing elements drawn at a high map extent may undisplay early as the user pans the map. To prevent this, draw and maintain drawing features at a lower map view.

**Drawing | Navigation with eraser activated:** Navigation is disabled when the eraser tool is activated. To resume map navigation, pan or zoom, disable the eraser tool and try again.

**Drawing | Tools disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in.

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase “I” may look like a lowercase “L.” Users can verify spelling if needed by changing the zoom level of the browser and temporarily zoom in to view the text.

**Firefox | Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Layer checker | Google mapping engine:** DisasterAWARE may not update the background layer’s status when ‘Google’ is set as the Preferred Mapping Engine.

**Print Service | Map Tips:** Map Tips are not supported for Prints at this time.

**Print Service | Paper size:** A0 and A1 paper sizes (poster sizes) are not currently available.

**Safari | Logout:** If a Safari user clicks the back arrow after logging out the application will temporarily be reloaded. We recommend using the Firefox or Chrome browsers instead.

**Safari | Save to File**: The bookmark option to “Save to File” feature is currently not available for Safari users.

**Smart Alert Notifications | Stop:** Where available, when a user replies “Stop” to a text (SMS)

Smart Alert, the response they receive is in English, regardless of which language is selected.

**Smart Alert Notifications | Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for Smart Alert notifications.

**Smart Alert Notifications | Assets:** Users who have their alert area set as “Global” but who have active Asset Providers may only receive notifications for hazards that intersect their active tracked Assets. To receive “Global” notifications, users should delete the Asset Providers and stop tracking assets.

**Table of contents | Freezing:** If the “Recent Layers” folder is open and all of a folder layers are enabled, then DisasterAWARE may not operate as expected and a user may not be able to navigate the Table of Contents from the Layers panel. A workaround is to make sure the “Recent Layers” folder is closed while navigating the Table of Contents and enabling/inspecting data layers.

**Virtual layers | Selection bar:** If users have select themes enabled (PDC, Light, or Dark) then the Virtual Layer bar at the bottom of the map viewer may not be easily used to determine the active selection. Users may pause the animation and manually choose a selection to determine which selection is actively displaying in the map viewer.

**Web Map Service (WMS) | Asynchronous requests:** When a user selects a location, a request is made in parallel to each enabled WMS layer to see if there is a feature at that location. The first layer to respond with a feature is the displayed layer. Making requests serially in an order defined by the visual order of the layers may increase response times unnecessarily.

**Web Map Service (WMS) | Coordinates:** WMS 1.3 specification flips coordinates in a northing then easting orientation (minY, minX, maxY, maxX) which differs than standard orientation (minX, minY, maxX, maxY). DisasterAWARE flips the coordinates for WMS 1.3 and projection 4326 to accommodate this behavior.

**Web Map Service (WMS) | Undeclared virtual layers:** Imported layers may appear as independent layers in the table of contents (ToC) and may display as both a child-of and independent layer in the Table of Contents. Layer inspection may differ depending on the layer(s) enabled when the inspection event occurs.

**Web Map Service (WMS) | Symbology:** Icons displayed in Map Tips and the Feature Inspector may not match the symbol displayed on the map.

**Web Map Service (WMS) | Feature list:** The Feature List for WMS services will only show features that the user discovered by clicking on the map. The feature list may not show all features that are on the map like other feature layers do.
