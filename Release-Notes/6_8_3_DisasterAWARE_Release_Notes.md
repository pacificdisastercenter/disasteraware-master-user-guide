**RELEASE NOTES**

**DisasterAWARE<sup>®</sup> v 6.8.3 |** Release Date: 21 June 2021

New Features 
============

**Print delete function:** A new function to delete a saved print is now available. Users can remove saved prints from their print list by choosing the delete option.

**Product refresh: A new feature has been added to refresh the product list inside the hazard panel. This allows users to manually check for new products instead of waiting for the timed system refresh function.**

Issues Resolved
===============

**DisasterAlert logout: Resolved an issue for DisasterAlert web users that logged in and out of the application multiple times in the same session.**

**Unavailable layer colors: Addressed an issue that resulted in some available layers appearing as unavailable/offline (orange text) in the Layer's list.**

Deprecated
==========

**Save hazard filter: The setting to save hazard filters has been removed from the user preferences panel.**

Known Issues
============

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief | Historical Hazards:** Historical hazard data is limited and may not match user expectations.

**Area Brief | Map:** The map displayed inside the *Infrastructure & Critical Facilities* section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Area Brief | RVA:** Risk & Vulnerability Assessment (RVA) layers presented may not match the legends in the report.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to [<span class="underline">http://disasteraware.pdc.org/help/How\_To\_Disable\_AdBlockers.pdf</span>](http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf)

**Burmese language on animated layers:** Burmese language does not translate on some animated layers with a time scale and instead display “NaNh.”

**Drawing | Large figures:** Drawing elements drawn at a high map extent may undisplay early as the user pans the map. To prevent this, draw and maintain drawing features at a lower map view.

**Drawing | Navigation with eraser activated:** Navigation is disabled when the eraser tool is activated. To resume map navigation, pan or zoom, disable the eraser tool and try again.

**Drawing | Tools disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in.

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase “I” may look like a lowercase “L.” Users can verify spelling if needed by changing the zoom level of the browser and temporarily zoom in to view the text.

**Fonts in DMRS**: The appearance of English user interface elements in DMRS may not appear correctly if users install & use the Google Chrome extension “Myanmar Font Tools” for ZAWGYI encoding standards. This issue only affects users on ZAWGYI computers.

**Firefox | Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Print Service | Map Tips:** Map Tips are not supported for Prints at this time.

**Print Service | Paper size:** A0 and A1 paper sizes (poster sizes) are not currently available.

**Safari | Logout:** If a Safari user clicks the back arrow after logging out the application will temporarily be reloaded. We recommend using the Firefox or Chrome browsers instead.

**Safari | Save to File**: The bookmark option to “Save to File” feature is currently not available for Safari users.

**SmartAlert Notifications | Stop:** Where available, when a user replies “Stop” to a text (SMS)

SmartAlert, the response they receive is in English, regardless of which language is selected.

**SmartAlert Notifications | Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for SmartAlert notifications.

**SmartAlert Notifications | Assets:** Users who have their alert area set as “Global” but who have active Asset Providers may only receive notifications for hazards that intersect their active tracked Assets. To receive “Global” notifications, users should delete the Asset Providers and stop tracking assets.
