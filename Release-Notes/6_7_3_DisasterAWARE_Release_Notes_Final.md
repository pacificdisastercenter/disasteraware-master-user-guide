Release Notes 
=============

**DisasterAWARE<sup>®</sup> v 6.7.3 |** Release Date: 10 November 2020

Issues resolved
---------------

**Cameras:** Cameras may stack to one side when the user has enabled more than 10.

**Identify | Summaries:** Summary data tables captured by the identify tool will now display a zero value instead of blank table cells.

**Identify Tool | Custom Selection:** The Identify tool options have been modified to become more user friendly. The Rectangle Selection option has been moved to the top of the panel followed by the Custom Selection tool (formerly the Polygon Selection tool).

**Language Options | Lao Language:** Fixed an issue that prevented users from selecting Lao as an available language option.

**Layers |** **Action menu**: Fixed an issue in which the action menu (3 dots at bottom of layers panel) does not disappear when users switch between landscape and portrait views on handheld devices.

**Layers | Hidden folders:** New DisasterAWARE users will now see system default folder preferences.

**Layers | User added:** Fixed an issue that occurred when users added custom map services that were already included in the Table of Contents (ToC) and prevented layers from drawing when selected.

Deprecated
----------

**Area Brief | Circle Selector:** The Circle selector tool has been removed and may not be used to create Area Brief reports.

Known Issues
------------

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief:** The map displayed inside the *Infrastructure & Critical Facilities* section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Area Brief:** Risk & Vulnerability Assessment (RVA) layers presented may not match the legends in the report.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to <http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf>

**Burmese language on animated layers**: Burmese language does not translate on some animated layers with a time scale and instead displays “NaNh.”

**Drawing tool disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in. 

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase “I” may look like a lowercase “L.” Users can verify spelling if needed by changing the zoom level of the browser and temporarily zoom in to view the text.

**Fonts in DMRS**: The appearance of English user interface elements in DMRS may not appear correctly if users install & use the Google Chrome extension “Myanmar Font Tools” for ZAWGYI encoding standards. This issue only affects users on ZAWGYI computers.

**Firefox | Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Internet Explorer 11 | Stability**: IE11 users may experience slowdowns when moving the map, opening menus, and opening products. Users may also experience intermittent program crashes.

**Internet Explorer 11 | Bookmarks:** Bookmark edits may not appear in IE 11 until the cache is cleared.

**Internet Explorer 11 | Attachments:** IE 11 administrators cannot change a product that has been uploaded (jpg, pdf or txt, for example). If a user cannot switch to a different browser or create a new product with the new file, hide the original product by adding it to the “Recycle Bin” parent folder.

**Internet Explorer 11 | Drawing tools:** In IE 11, the Free Line and Free Polygon tools may shift away from their intended location (cursor position) while being drawn.

**Internet Explorer 11 | InPrivate browsing**: The product list preview will not appear with private browsing enabled (IE InPrivate browsing mode).

**Safari | Logout:** If a Safari user clicks the back arrow after logging out the application will temporarily be reloaded. We recommend using the Firefox or Chrome browsers instead.

**Safari | Save to File**: The bookmark option to “Save to File” feature is currently not available for Safari users.

**SmartAlert Notifications | Stop:** When a user replies “Stop” to a text (SMS) SmartAlert, the response they receive is in English, regardless of which language is selected.

**SmartAlert Notifications | Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for SmartAlert notifications.

**SmartAlert Notifications | Assets:** Users who have their alert area set as “Global” but who have active Asset Providers may only receive notifications for hazards that intersect their active tracked Assets. To receive “Global” notifications, users should delete the Asset Providers and stop tracking assets.

**User Panel | Account:** On the Account section of the User panel, the organization and title is not saved. Users may replace, but may not delete phone numbers after they are linked to an account.
