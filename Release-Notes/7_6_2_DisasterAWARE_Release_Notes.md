**RELEASE NOTES**

**DisasterAWARE<sup>®</sup> v 7.6.2 |** Release Date: 23 May 2023

# Issues Resolved

**Bookmarks | Editing:** The Home menu option is now disabled while editing bookmarks to prevent inconsistencies.

**Bookmarks | Import:** Fixed an issue where session imported bookmarks are inappropriately displayed in the Default Bookmark Selection list.

**Cameras | Visit URL:** Resolved an issue with cameras in which the option to visit the source location does not redirect appropriately.

**Drawing | Line Base:** Fixed an issue where colored drawing figures are displayed with a black base.

**Hazards | Editing:** DisasterAWARE admin will no longer observe a false error message when needing to edit automated hazard details.

**Hazards | Icon Loading:** Fixed an issue with the DisasterAWARE system loading that was triggered when a hazard icon failed to load. DisasterAWARE will now continue to load and use the default unknown hazard icon.

**Localization:** Several improvements were made to DisasterAWARE to support translations including, but not limited to the drawing panel empty list text, search bars, camera counts, and area brief title.

**Map | Ruler:** Made improvements to the map ruler feature to resolve an issue where the ruler details would not display until the map was panned.

**Mapping Solution:** Fixed a loading issue with the Preferred Mapping Engine user preference that occurred when using a mobile web browser.

**Preferences | Products:** The ‘Display Hidden Products’ user preference will now work when selected from the Product action menu.

**Print | Annotations:** Long annotations saved in Print products should no longer be truncated.

**Print | Loading Spinner:** Fixed an issue with the spinning loading graphic.

**Shapefiles | Style Files:** Made improvements to support styles and added basic support for shapefiles in DisasterAWARE.

# Known Issues

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief | Historical Hazards:** Historical hazard data is limited and may not match user expectations.

**Area Brief | Map:** The map displayed inside the Infrastructure & Critical Facilities section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Area Brief | RVA:** Risk & Vulnerability Assessment (RVA) layers presented may not match the legends in the report.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a hazard, cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to [<u>http://disasteraware.pdc.org/help/How\_To\_Disable\_AdBlockers.pdf</u>](http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf)

**Burmese language on animated layers:** Burmese language does not translate on some animated layers with a time scale and instead displays “NaNh.”

**Camera streaming:** Some service streaming cameras configured in DisasterAWARE (e.g. Ozolio) may display the camera previews with larger black margins above and below the camera stream while enabled and viewed on the Active Cameras panel.

**Drawing | Anti-meridian:** Drawing elements in bookmarks that cross the 180<sup>th</sup> meridian may not copy appropriately if duplicated using the bookmark figure inspector.

**Drawing | Large figures:** Drawing elements drawn at a high map extent may stop displaying as the user pans the map. To prevent this, draw and maintain drawing features at a lower map view.

**Drawing | Navigation with eraser activated:** Navigation is disabled when the eraser tool is activated. To resume map navigation, pan or zoom, or disable the eraser tool and try again.

**Drawing | Tools disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Domain Change | Logout:** If a user logs in to Disaster Alert before changing their configured Domain then when opts to Log Out and return to Disaster Alert their logged in session will be expired and the user will need to log back in to Disaster Alert.

**FHP | Hazard Exposure:** Hazard Exposure details may be slightly slower to load in the FHP Active Hazards view.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem, please zoom in.

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase “I” may look like a lowercase “L.” Users can verify spelling if needed by changing the zoom level of the browser and temporarily zooming in to view the text.

**Firefox | Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a hazard may need to refresh their browser to see the hazard expire from the map.

**Imported layers:** If the user attempts to import a Map Service that is already configured in DisasterAWARE then the name of the imported layers may not appear according to the import configuration but rather the existing configuration for the Map Service.

**Language change:** Changing languages in DisasterAWARE may take longer than expected for the app to translate to the selected language. A workaround is to: 1). Select desired language, 2). Select English, 3). Brief pause, 4). Select desired language.

**Layer checker | Google mapping engine:** DisasterAWARE may not update the background layer’s status when ‘Google’ is set as the Preferred Mapping Engine.

**Layer metadata:** Some externally hosted services may display old ‘Updated’ timestamps. The layer abstract text contains the update frequency. Additionally, metadata links may not redirect to GHIN appropriately for all layers. These are anticipated to be resolved with the Digital Data Library.

**Map | Ruler:** If DisasterAWARE is loaded in mobile portrait view and the map rulers are enabled then the top bar ruler will not display on the active screen while a menu is enabled. To display the top ruler please close the open menu.

**Offline | Layer cache:** Layers will not draw in offline mode unless they are enabled and drawn while the app is connected to the internet to cache the layer feature data.

**Print Service | Annotations:** There is a minor difference in font style used for annotations on created print products. Longer annotation features may display in produced Print Products with the end cut off.

**Print Service | Layers:** Due to potential configuration differences it is recommended to only enable layers desired in completed Print products. Please disable other not-drawn layers from the DisasterAWARE Table of Contents before creating the print product. This can help prevent unexpected layers from displaying in the finished Print Product. Additionally, some layers may not be applied as expected in print products.

**Print Service | Map Background:** Google base maps are not supported in Print Products. If a print is submitted with a Google Basemap a Bing map will be automatically applied. This may slightly change the appearance of the created product and vary from user expectations.

**Print Service | Map Tips:** Map Tips are not supported for Print functions at this time.

**Print Service | Paper size:** A0 and A1 paper sizes (poster sizes) are not currently available.

**Products:** If a hazard contains more than 1,000 products, there may be a longer-than-expected delay in the list of products in the Products Panel.

**RVA Data:** Data is not rounding or displaying with the percent sign (%) as expected. This is impacting newer data added to the system beginning in 2022.

**RVA Data | No Data:** The legend in DisasterAWARE may be missing the ‘No Data’ legend entry. This data category is drawn as a grey color in the map viewer. The layer draws as expected and the rest of the information is available in the legend (except the ‘No Data’ entry). Please see the example of the Grey result below. In DisasterAWARE, you can reference “Western Sahara” in Africa, as well.

**Safari | Logout:** If a Safari user clicks the back arrow after logging out, the application will temporarily be reloaded. We recommend using the Firefox or Chrome browsers instead.

**Safari | Save to File**: The bookmark option “Save to File” is currently not available for Safari users.

**Smart Alert | Email Verification:** When a user changes their configured Smart Alert email the default email of the email account registered remains active if the new email is not verified through the confirmation email link.

**Smart Alert | SMS Update:** Updates to the user account phone number will overwrite the user's configured Smart Alert SMS number. If a user needs to update their registered account phone number, they should verify the change.

**Smart Alert Notifications | Stop:** Where available, when a user replies “Stop” to a text (SMS) Smart Alert, the response they receive is in English, regardless of which language is selected.

**Smart Alert Notifications | Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for Smart Alert notifications.

**Smart Alert Notifications | Assets:** Users who have their alert area set as “Global” but who have active Asset Providers may only receive notifications for hazards that intersect their actively tracked assets. To receive “Global” notifications, users should delete the Asset Providers and stop tracking assets.

**Table of contents | Freezing:** If the “Recent Layers” folder is open and all the folder layers are enabled, then DisasterAWARE may not operate as expected and users may not be able to navigate the Table of Contents from the Layers panel. One workaround is to make sure the “Recent Layers” folder is closed while navigating the Table of Contents and inspecting data layers.

**Virtual layers | Selection bar:** If users have themes enabled (PDC, Light, or Dark), the Virtual Layer bar at the bottom of the map viewer may not be easily used to determine the active selection. Users may pause the animation and manually choose a selection to determine which selection is actively displaying in the map viewer.

**Web Feature Service (WFS) | Feature count:** WFS feature counts are limited to 100 for bandwidth concerns.

**Web Feature Service (WFS) | Layer style:** WFS does not provide style support and layers are drawn with default Icons.

**Web Map Service (WMS) | Asynchronous requests:** When a user selects a location, a request is made in parallel to each enabled WMS layer to see if there is a feature at that location. The first layer to respond with a feature is the displayed layer. Making requests serially in an order defined by the visual order of the layers may increase response times unnecessarily.

**Web Map Service (WMS) | Coordinates:** WMS 1.3 specification flips coordinates in a North and then East orientation (minY, minX, maxY, maxX), which differs from a standard orientation (minX, minY, maxX, maxY). DisasterAWARE flips the coordinates for WMS 1.3 and projection 4326 to accommodate this behavior.

**Web Map Service (WMS) | Cursor hover:** The cursor will not change from the hand symbol to the pointer symbol when WMS features are hovered over in DisasterAWARE. Map tips will also not display on feature hover.

**Web Map Service (WMS) | Undeclared virtual layers:** Imported layers may appear as independent layers in the Table of Contents (ToC) and may display as both a child of, and independent layer in the Table of Contents. Layer inspection may differ depending on the layer(s) enabled when the inspection event occurs.

**Web Map Service (WMS) | Symbology:** Icons displayed in Map Tips and the Feature Inspector may not match the symbol displayed on the map.

**Web Map Service (WMS) | Feature list:** The Feature List for WMS services will only show features that the user discovered by clicking on the map. The feature list may not show all features that are on the map as other feature layers do.

**Zoom To Tool:** Users who click the map quickly multiple times with the Zoom To tool enabled (magnifying tool option from the right-side navigation menu), may experience undesired behavior. The map will not tile appropriately at the new zoom level. When the user clicks “Home” from the right-side navigation tools, the hazard clusters will appear expanded.
