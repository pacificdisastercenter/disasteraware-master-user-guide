**RELEASE NOTES**

**DisasterAWARE<sup>®</sup> v 8.0.10 |** Release Date: 23 Apr 2024

# Issues Resolved 

**Exposure Report | Properties:** Fixed an issue with the Asset Report in which only the first property would display. All selected assets exposed are now displayed in the report.

**Layer | Winds:** Fixed an issue that resulted in the Winds (Surface Velocity & Forecast) layer not displaying as expected when enabled.

# Known Issues

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Browser Ad Blockers:** Supplemental information products such as situation reports, map graphics, and other analytical products that are associated with a hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to [<u>http://disasteraware.pdc.org/help/How\_To\_Disable\_AdBlockers.pdf</u>](http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf)

**Drawing | Large figures:** Drawing elements drawn at a high map extent may stop displaying as the user pans the map. To prevent this, draw and maintain drawing features at a lower map extent.

**Drawing | Navigation with eraser activated:** Navigation is disabled when the eraser tool is activated. To resume map navigation, pan or zoom, or disable the eraser tool and try again.

**Drawing | Tools disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Domain Change | Logout:** If a user logs in to Disaster Alert before changing their configured Domain then when opts to Log Out and return to Disaster Alert their logged in session will be expired and the user will need to log back in to Disaster Alert.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem, please zoom in.

**Imported layers:** If the user attempts to import a Map Service that is already configured in DisasterAWARE then the name of the imported layers may not appear according to the import configuration but rather the existing configuration for the Map Service.

**Language change:** Changing languages in DisasterAWARE may take longer than expected for the app to translate to the selected language. A workaround is to: 1) Select desired language; 2) Select English; 3) Brief pause; or 4) Select the desired language.

**Layer checker | Google mapping engine:** DisasterAWARE may not update the background layer’s status when ‘Google’ is set as the Preferred Mapping Engine.

**Print Service | Layers:** Due to potential configuration differences it is recommended to only enable layers desired in completed Print products. Please disable other not-drawn layers from the DisasterAWARE Table of Contents before creating the print product. This can help prevent unexpected layers from displaying in the finished Print Product. Additionally, some layers may not be applied as expected in print products.

**Print Service | Map Background:** Google base maps are not supported in Print Products. If a print is submitted with a Google Basemap a Bing map will be automatically applied. This may slightly change the appearance of the created product and vary from user expectations.

**Print Service | Map Tips:** Map Tips are not supported for Print functions at this time.

**Print Service | Paper size:** A0 and A1 paper sizes (poster sizes) are not currently available.

**RVA Data:** Data is not rounding or displaying with the percent sign (%) as expected. This is impacting newer data added to the system beginning in 2022.

**RVA Data | No Data:** The legend in DisasterAWARE may be missing the ‘No Data’ legend entry. This data category is drawn as a grey color in the map viewer. The layer draws as expected and the rest of the information is available in the legend (except the ‘No Data’ entry). In DisasterAWARE, you can reference “Western Sahara” in Africa, as well.

**Smart Alert | Email Verification:** When a user changes their configured Smart Alert email, the default email of the email account registered remains active if the new email is not verified through the confirmation email link.

**Smart Alert Notifications | Assets:** Users who have their alert area set to “Global,” yet also have active Asset Providers, may only receive notifications for hazards that intersect their actively tracked assets. To receive “Global” notifications, users should delete the Asset Providers and stop tracking assets.

**Table of contents | Freezing:** If the “Recent Layers” folder is open and all the folder layers are enabled, DisasterAWARE may not operate as expected and users may not be able to navigate the Table of Contents from the Layers panel. One workaround is to make sure the “Recent Layers” folder is closed while navigating the Table of Contents and inspecting data layers.

**Virtual layers | Selection bar:** If users have themes enabled (PDC, Light, or Dark), the Virtual Layer bar at the bottom of the map viewer may not be easily used to determine the active selection. Users may pause the animation and manually choose a selection to determine which selection is actively displaying in the map viewer.

**Web Feature Service (WFS) | Feature count:** WFS feature counts are limited to 100 for bandwidth concerns.

**Web Feature Service (WFS) | Layer style:** WFS does not provide style support and layers are drawn with default Icons.

**Web Map Service (WMS) | Asynchronous requests:** When a user selects a location, a request is made in parallel to each enabled WMS layer to see if there is a feature at that location. The first layer to respond with a feature is the displayed layer. Making requests serially in an order defined by the visual order of the layers may increase response times unnecessarily.

**Web Map Service (WMS) | Coordinates:** WMS 1.3 specification flips coordinates in a North and then East orientation (minY, minX, maxY, maxX), which differs from a standard orientation (minX, minY, maxX, maxY). DisasterAWARE flips the coordinates for WMS 1.3 and projection 4326 to accommodate this behavior.

**Web Map Service (WMS) | Cursor hover:** The cursor will not change from the hand symbol to the pointer symbol when WMS features are hovered over in DisasterAWARE. Map tips will also not display on feature hover.

**Web Map Service (WMS) | Undeclared virtual layers:** Imported layers may appear as independent layers in the Table of Contents (ToC) and may display as both a child of, and independent layer in the Table of Contents. Layer inspection may differ depending on the layer(s) enabled when the inspection event occurs.

**Web Map Service (WMS) | Symbology:** Icons displayed in Map Tips and the Feature Inspector may not match the symbol displayed on the map.

**Web Map Service (WMS) | Feature list:** The Feature List for WMS services will only show features that the user discovered by clicking on the map. The feature list may not show all features that are on the map as other feature layers do.

**Zoom To Tool:** Users who click the map quickly multiple times with the Zoom To tool enabled (magnifying tool option from the right-side navigation menu), may experience undesired behavior. The map will not tile appropriately at the new zoom level. When the user clicks “Home” from the right-side navigation tools, the hazard clusters will appear expanded.
